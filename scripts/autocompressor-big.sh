#!/bin/sh

# tag::doc[]
# 
# end::doc[]

set -eu

echo "DEPRECATED. Use autocompressor.sh instead. This script will be removed in v0.5"

exit 1
