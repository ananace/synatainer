#!/bin/sh

# tag::doc[]
# Resets the autocompressor for a room
#
# Usage: autocompressor_reset_room.sh room_id
#
# WARNING: Mileage can vary on side effects.
#
# end::doc[]

set -eu

if [ -z "${1:-}" ]; then
  echo "Usage: autocompressor_reset_room.sh room_id"
  exit 1
fi

sqlstr="$(stui pgconnectstring)"

psql -d "$sqlstr" -c "DELETE FROM state_compressor_state WHERE room_id='${1}';"
psql -d "$sqlstr" -c "DELETE FROM state_compressor_progress WHERE room_id='${1}';"
