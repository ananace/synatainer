#!/bin/sh

# tag::doc[]
# 
# end::doc[]

set -eu

sqlstr="$(stui pgconnectstring)"

synapse_auto_compressor -p "${sqlstr}" -n ${STATE_AUTOCOMPRESSOR_CHUNKS_TO_COMPRESS:-100} -c ${STATE_AUTOCOMPRESSOR_CHUNK_SIZE:-500}
