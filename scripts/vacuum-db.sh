#!/bin/sh

# tag::doc[]
# Vacuum the database
#   Set VACUUM_DB=skip to not vacuum the database
# end::doc[]

set -eu

echo "vacuum-db.sh is deprecated. Use stui vacuum-db [--help]"

case "${VACUUM_DB:-yes}" in
  none|off|skip ) echo "Vacuuming database turned off: (${VACUUM_DB}). Bye."
                  exit 0
                  ;;
esac

sqlstr="$(stui pgconnectstring)"

for table in $(psql -d "$sqlstr" --tuples-only -P pager=off -c "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public';"); do
  psql -d "$sqlstr" -c "VACUUM FULL VERBOSE $table;"
done
