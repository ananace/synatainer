#!/bin/sh

# tag::doc[]
# Opens a db shell (psql)
# end::doc[]

set -eu

psql -d "$(stui pgconnectstring)"
