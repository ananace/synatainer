#!/bin/sh

# tag::doc[]
# Resets the autocompressor
#
# WARNING: This just drops the state compressor tables from the database. Mileage can vary on side effects.
# end::doc[]

set -eu

sqlstr="$(stui pgconnectstring)"

psql -d "$sqlstr" -c "DROP TABLE state_compressor_state;"
psql -d "$sqlstr" -c "DROP TABLE state_compressor_progress;"
psql -d "$sqlstr" -c "DROP TABLE state_compressor_total_progress;"
