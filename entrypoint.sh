#!/bin/sh

set -eu

# check the PGPASSFILE for proper configuration & permissions
if [ -n "${PGPASSFILE:-}" ]; then
  if [ ! -f ${PGPASSFILE} ]; then
    echo "Configured PGPASSFILE=${PGPASSFILE} does not exist." >&2
    exit 2
  fi
fi

if [ -z "${PGPASSFILE:-}" -a -f /conf/pgpassword ]; then
  PGPASSFILE=/conf/pgpassword
  export PGPASSFILE
fi

if [ -n "${PGPASSFILE:-}" ]; then
  # check permissions
  if [ $(stat -c "%a" ${PGPASSFILE}) != "600" ]; then
    chmod 600 ${PGPASSFILE}
  fi

  #check ownership
  if [ $(id -u) -eq 0 ]; then
    uid=65534
  else
    uid=$(id -u)
  fi

  if [ $(stat -c "%u:%g" ${PGPASSFILE}) != "${uid}:${uid}" ]; then
    chown ${uid}:${uid} ${PGPASSFILE}
  fi
fi

# check the MXPASSFILE for proper configuration & permissions
if [ -n "${MXPASSFILE:-}" ]; then
  if [ ! -f ${MXPASSFILE} ]; then
    echo "Configured MXPASSFILE=${MXPASSFILE} does not exist." >&2
    exit 2
  fi
fi

if [ -z "${MXPASSFILE:-}" -a -f /conf/mxtoken ]; then
  MXPASSFILE=/conf/mxtoken
  export MXPASSFILE
fi

if [ -n "${MXPASSFILE:-}" ]; then
  # check permissions
  if [ $(stat -c "%a" ${MXPASSFILE}) != "600" ]; then
    chmod 600 ${MXPASSFILE}
  fi

  #check ownership
  if [ $(id -u) -eq 0 ]; then
    uid=65534
  else
    uid=$(id -u)
  fi

  if [ $(stat -c "%u:%g" ${MXPASSFILE}) != "${uid}:${uid}" ]; then
    chown ${uid}:${uid} ${MXPASSFILE}
  fi
fi

# if a command was given, execute it
if [ -n "$*" ]
then
  # if run as root (uid 0), downgrade yourself to nobody (uid 65534)
  if [ $(id -u) -eq 0 ]
  then
    exec su -s /bin/sh nobody -c "$*"
  else
    exec "$@"
  fi
fi

# no command given, setup the background service

su -s /bin/sh nobody -c "/setup-crontab.sh"

# start crond

crond -f -l 8

echo "If you can read this, something went terrible wrong."
