package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"text/tabwriter"

	"gitlab.com/mb-saces/synatainer/stui/mxadmin"
	"maunium.net/go/mautrix/id"
)

func doRoomCmd(cmd string, room string) {
	if strings.HasPrefix("detail", cmd) {
		doRoomDetails(id.RoomID(room))
	} else if strings.HasPrefix("members", cmd) {
		doRoomMembers(id.RoomID(room))
	} else {
		fmt.Printf("Not a valid command: '%s'. Valid commands are: details members.\nCommands can be abbr.\n", cmd)
	}
}

func doRoomDetails(room id.RoomID) {

	acli, err := MXAdminLogin()

	if err != nil {
		log.Fatalf("Failed to login: %v\n", err)
		return
	}
	detail, err := acli.RoomDetail(id.RoomID(room))
	if err != nil {
		log.Fatalf("error while command: %v\n", err)
		return
	}
	fmt.Printf("Details for room: '%s'\n", room)
	printRoomDetails(detail)
	fmt.Println("Done.")
}

func printRoomDetails(detail *mxadmin.RespRoomDetail) {

	w := tabwriter.NewWriter(os.Stdout, 6, 8, 1, ' ', 0)

	fmt.Fprintf(w, "RoomID:\t%s\n", detail.RoomID)
	fmt.Fprintf(w, "Name:\t%s\n", detail.Name)
	fmt.Fprintf(w, "Avatar:\t%s\n", detail.Avatar)
	fmt.Fprintf(w, "Topic:\t%s\n", detail.Topic)
	fmt.Fprintf(w, "CanonicalAlias:\t%s\n", detail.CanonicalAlias)
	fmt.Fprintf(w, "Joined_members:\t%d\n", detail.Joined_members)
	fmt.Fprintf(w, "Joined_local_members:\t%d\n", detail.Joined_local_members)
	fmt.Fprintf(w, "Joined_local_devices:\t%d\n", detail.Joined_local_devices)
	fmt.Fprintf(w, "Version:\t%s\n", detail.Version)
	fmt.Fprintf(w, "Creator:\t%s\n", detail.Creator)
	fmt.Fprintf(w, "Encryption:\t%s\n", detail.Encryption)
	fmt.Fprintf(w, "Federatable:\t%t\n", detail.Federatable)
	fmt.Fprintf(w, "Public:\t%t\n", detail.Public)
	fmt.Fprintf(w, "Join_rules:\t%s\n", detail.Join_rules)
	fmt.Fprintf(w, "Guest_access:\t%s\n", detail.Guest_access)
	fmt.Fprintf(w, "History_visibility:\t%s\n", detail.History_visibility)
	fmt.Fprintf(w, "State_events:\t%d\n", detail.State_events)

	w.Flush()
}

func doRoomMembers(room id.RoomID) {

	acli, err := MXAdminLogin()

	if err != nil {
		log.Fatalf("Failed to login: %v\n", err)
		return
	}
	members, err := acli.RoomMembers(id.RoomID(room))
	if err != nil {
		log.Fatalf("Error while command: %v\n", err)
		return
	}
	fmt.Printf("Members of room '%s': %d\n", string(room), members.Total)
	for _, m := range members.Members {
		fmt.Printf("%s\n", m)
	}
}
