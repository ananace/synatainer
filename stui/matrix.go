package main

import (
	"gitlab.com/mb-saces/synatainer/stui/confutils"
	"gitlab.com/mb-saces/synatainer/stui/mxadmin"
	"gitlab.com/mb-saces/synatainer/stui/mxutils"
)

func Resolve_MX_Token() (mxtoken string, err error) {

	resolveConfigItemString(&mx_synapse_host, "SYNAPSE_HOST", "http://localhost:8008")
	resolveConfigItemString(&mx_token_file, "MXPASSFILE", "")
	resolveConfigItemString(&mx_bearer_token, "BEARER_TOKEN", "")

	if mx_token_file == "" && mx_bearer_token == "" {
		if confutils.FileExists("/conf/mxtoken") {
			mx_token_file = "/conf/mxtoken"
		}
	}

	if len(mx_token_file) > 0 {
		var pf *mxutils.Passfile
		pf, err = mxutils.ReadPassfile(mx_token_file)
		if err != nil {
			return
		}
		mxtoken = pf.FindPassword(mx_synapse_host, "", "")
	} else {
		mxtoken = mx_bearer_token
	}
	return
}

// MXAdminLogin
func MXAdminLogin() (mxaclient *mxadmin.AdminClient, err error) {

	var mxtoken string

	mxtoken, err = Resolve_MX_Token()

	if err != nil {
		return
	}

	mxaclient, err = mxadmin.NewAdminClient(mx_synapse_host, "", mxtoken)

	if err != nil {
		return
	}

	_, err = mxaclient.Whoami()

	if err != nil {
		return
	}
	return
}
