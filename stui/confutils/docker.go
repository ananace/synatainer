package confutils

import "os"

// check if we are running in a container
func CheckContainer() string {
	_, ok := os.LookupEnv("KUBERNETES_SERVICE_HOST")
	if ok {
		return "k8s"
	}
	if FileExists("/.dockerenv") {
		return "docker"
	}
	if FileExists("/run/.containerenv") {
		return "podman"
	}
	return ""
}
