package main

func autocompressor() {
	resolveDBConfig()
	resolveConfigItemString(&sac_chunks_to_compress, "STATE_AUTOCOMPRESSOR_CHUNKS_TO_COMPRESS", "100")
	resolveConfigItemString(&sac_chunk_size, "STATE_AUTOCOMPRESSOR_CHUNK_SIZE", "500")
	//	synapse_auto_compressor -p "user=$DB_USER password=$PGPASSWORD dbname=$DB_NAME host=$DB_HOST" -n ${STATE_AUTOCOMPRESSOR_CHUNKS_TO_COMPRESS:-100} -c ${STATE_AUTOCOMPRESSOR_CHUNK_SIZE:-500}
	simpleExec("synapse_auto_compressor", "-p", getPostgresConnectionString(), "-n", sac_chunks_to_compress, "-c", sac_chunk_size)
}
