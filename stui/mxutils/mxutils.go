package mxutils

import (
	"errors"
	"log"
	"strings"

	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/id"
)

// UnEmailHandle Takes a matrix handle, either in matrix or e-mail
// notation, and returns the handle in matrix notation
func UnEmailHandle(h string) string {
	// check for empty string
	if h == "" {
		return ""
	}
	// check for sigil
	if strings.ContainsAny(h[0:1], "@!#") {
		if i := strings.LastIndex(h[1:], "@"); i > 0 {
			return h[0:i+1] + ":" + h[i+2:]
		}
	} else {
		// no sigil, assume email address as mxid
		if i := strings.LastIndex(h, "@"); i > 0 {
			return "@" + h[0:i] + ":" + h[i+1:]
		}

	}
	return h
}

// MXLogin
func MXLogin(username string, token string) (mxclient *mautrix.Client, err error) {
	log.Println("user pass Login " + username)

	mxid := id.UserID(UnEmailHandle(username))

	_, hs, err := mxid.Parse()
	if err != nil {
		return
	}
	log.Println("HS: " + hs)
	wk, err := mautrix.DiscoverClientAPI(hs)
	log.Println("testinger")
	log.Println(wk)
	log.Println(err)
	if err != nil {
		return
	}
	if wk != nil {
		hs = wk.Homeserver.BaseURL
	}
	log.Println("HS URL: " + hs)
	//log.Println(wk)
	mxclient, err = mautrix.NewClient(hs, id.UserID(username), token)

	//if err != nil {
	//	return
	//}
	//err = errors.New("MXLogin not implemented yet.")
	return
}

func MXRoomAlias(alias string) (mxalias id.RoomAlias, err error) {
	err = errors.New("MXRoomAlias not implemented yet.")
	return
}

func MXUserID(username string) (mxuser id.UserID, err error) {
	err = errors.New("MXUserID not implemented yet.")
	return
}

func MXRoom(room string) (mxroom id.RoomID, err error) {
	err = errors.New("MXRoom not implemented yet.")
	return
}
