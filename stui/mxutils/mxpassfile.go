package mxutils

import (
	"bufio"
	"io"
	"os"
	"regexp"
	"strings"
)

// inspired by https://github.com/jackc/pgpassfile

// Entry represents a line in a MX passfile.
type Entry struct {
	Synapsehost string
	Localpart   string
	Domain      string
	Token       string
}

// Passfile is the in memory data structure representing a MX passfile.
type Passfile struct {
	Entries []*Entry
}

// ReadPassfile reads the file at path and parses it into a Passfile.
func readPassfile(path string) (*Passfile, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return ParsePassfile(f)
}

// ParsePassfile reads r and parses it into a Passfile.
func ParsePassfile(r io.Reader) (*Passfile, error) {
	passfile := &Passfile{}

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		entry := parseLine(scanner.Text())
		if entry != nil {
			passfile.Entries = append(passfile.Entries, entry)
		}
	}

	return passfile, scanner.Err()
}

// Match (not colons or escaped colon or escaped backslash)+. Essentially gives a split on unescaped
// colon.
var colonSplitterRegexp = regexp.MustCompile("(([^|]|(\\|)))+")

// parseLine parses a line into an *Entry. It returns nil on comment lines or any other unparsable
// line.
func parseLine(line string) *Entry {
	const (
		tmpBackslash = "\r"
		tmpPipe      = "\n"
	)

	line = strings.TrimSpace(line)

	if strings.HasPrefix(line, "#") {
		return nil
	}

	line = strings.Replace(line, `\\`, tmpBackslash, -1)
	line = strings.Replace(line, `\|`, tmpPipe, -1)

	parts := strings.Split(line, "|")
	if len(parts) != 4 {
		return nil
	}

	// Unescape escaped colons and backslashes
	for i := range parts {
		parts[i] = strings.Replace(parts[i], tmpBackslash, `\`, -1)
		parts[i] = strings.Replace(parts[i], tmpPipe, `|`, -1)
	}

	return &Entry{
		Synapsehost: parts[0],
		Localpart:   parts[1],
		Domain:      parts[2],
		Token:       parts[3],
	}
}

// FindPassword finds the password for the provided synapsehost, localpart, and domain. An empty
// string will be returned if no match is found.
func (pf *Passfile) FindPassword(synapsehost, localpart, domain string) string {
	for _, e := range pf.Entries {
		if (e.Synapsehost == "*" || e.Synapsehost == synapsehost) &&
			(e.Localpart == "*" || e.Localpart == localpart) &&
			(e.Domain == "*" || e.Domain == domain) {
			return e.Token
		}
	}
	return ""
}
