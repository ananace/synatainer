package main

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/mb-saces/synatainer/stui/mxadmin"
)

func doRoomsCmd(cmd string) {
	if strings.HasPrefix("list", cmd) {
		listRooms()
	} else {
		fmt.Printf("Not a valid command: '%s'. Valid commands are: list.\nCommands can be abbr.\n", cmd)
	}
}

type listRoomCallback func(*mxadmin.RespRoomDetail)

func listAllRooms(mxaclient *mxadmin.AdminClient, callback listRoomCallback) {

	innerListRooms := func(from int) int {
		roomlist, err := mxaclient.ListRooms(from, 1000)
		if err != nil {
			log.Fatalf("error while command: %v\n", err)
		}
		for _, m := range roomlist.Rooms {
			callback(&m)
		}
		return roomlist.NextBatch
	}

	next := innerListRooms(0)

	for next > 0 {
		next = innerListRooms(next)
	}
}

func listRooms() {
	acli, err := MXAdminLogin()

	if err != nil {
		log.Fatalf("Failed to login: %v\n", err)
		return
	}

	simplePrintRoom := func(roomDetail *mxadmin.RespRoomDetail) {
		fmt.Printf("%s\n", roomDetail.RoomID)
	}

	listAllRooms(acli, simplePrintRoom)

}
