package main

import (
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/joho/godotenv"
	"gitlab.com/mb-saces/synatainer/stui/confutils"
)

// Make a variable for the config filename envirionment variable name, can be set at build time.
var cfgFileEnvName = "SYNATAINER_CONFIGFILE"

// did we load the config files?
var isLoaded bool = false

// settings from config file
var confFileEnv map[string]string

func simpleLoadConfig(filename string) map[string]string {
	envMap, err := godotenv.Read(filename)
	if err != nil {
		panic(err)
	}
	return envMap
}

func loadConfigFile() {
	if isLoaded {
		return
	}

	if len(configFile) == 0 {
		val, ok := os.LookupEnv("SYNATAINER_CONFIGFILE")
		if ok {
			configFile = val
		}
	}

	if len(configFile) > 0 {
		// explicit config file, load and ignore defaults
		confFileEnv = simpleLoadConfig(configFile)
		isLoaded = true
		return
	}

	if confutils.CheckContainer() != "" {
		cfgFile := "/conf/synatainer.conf"
		if confutils.FileExists((cfgFile)) {
			confFileEnv = simpleLoadConfig(cfgFile)
		} else {
			confFileEnv = make(map[string]string)
		}
		isLoaded = true
		return
	}

	// still here? load desktop conf...
	globalCfgName := "/etc/synatainer/synatainer.conf"
	userCfgName := path.Join(getUserConfigDir(), "synatainer/synatainer.conf")

	globalExists := confutils.FileExists(globalCfgName)
	userExists := confutils.FileExists(userCfgName)

	if globalExists && userExists {
		// booth files exist, load an merge them
		confFileEnv = simpleLoadConfig(globalCfgName)
		userEnv := simpleLoadConfig(userCfgName)
		for k, v := range userEnv {
			confFileEnv[k] = v
		}
	} else if globalExists {
		confFileEnv = simpleLoadConfig(globalCfgName)
	} else if userExists {
		confFileEnv = simpleLoadConfig(userCfgName)
	} else {
		confFileEnv = make(map[string]string)
	}

	isLoaded = true
}

func setStringPointerVar(variable interface{}, value string) {
	v, _ := (variable).(*string)
	*v = value
}

func resolveConfigItemString(cmdLineItem *string, envItem string, defaultValue string) {
	if len(*cmdLineItem) > 0 {
		// already set from comaandline, nothing to do.
		return
	}
	// not set on cmd line, look up env
	val, ok := os.LookupEnv(envItem)
	if ok {
		setStringPointerVar(cmdLineItem, val)
		return
	}
	// still not set, look up config file
	// ensure config file is loaded
	loadConfigFile()
	val, ok = confFileEnv[envItem]
	if ok {
		setStringPointerVar(cmdLineItem, val)
		return
	}
	// still here? return default
	//cmdLineItem = &defaultValue
	setStringPointerVar(cmdLineItem, defaultValue)
}

func resolveDBConfig() {
	resolveConfigItemString(&db_user, "DB_USER", "synapse_user")
	resolveConfigItemString(&db_host, "DB_HOST", "postgres:5432")
	resolveConfigItemString(&db_name, "DB_NAME", "synapse")
	resolveConfigItemString(&db_pass, "PGPASSWORD", "")
	resolveConfigItemString(&db_passfile, "PGPASSFILE", "")
	if db_pass == "" && db_passfile == "" {
		if confutils.FileExists("/conf/pgpassword") {
			db_passfile = "/conf/pgpassword"
		}
	}
}

func print_pgpass_line() {
	resolveDBConfig()
	fmt.Printf("*:*:%s:%s:%s\n", db_name, db_user, db_pass)
}

func escapeLine(line string) (s string) {
	const (
		tmpBackslash = "\r"
		tmpColon     = "\n"
	)
	s = strings.Replace(line, `\`, tmpBackslash, -1)
	s = strings.Replace(s, `|`, tmpColon, -1)
	s = strings.Replace(s, tmpBackslash, `\\`, -1)
	s = strings.Replace(s, tmpColon, `\|`, -1)
	return
}

func print_mxpass_line() {
	resolveConfigItemString(&mx_bearer_token, "BEARER_TOKEN", "ReplaceWithRealToken")
	resolveConfigItemString(&mx_synapse_host, "SYNAPSE_HOST", "http://localhost:8008")
	tmp_host := escapeLine(mx_synapse_host)
	fmt.Printf("%s|*|*|%s\n", tmp_host, mx_bearer_token)
}

func print_mxtoken() {
	token, err := Resolve_MX_Token()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error while resolving matrix token: %v", err)
		return
	}
	fmt.Println(token)
}

func getPostgresConnectString() (pgstring string) {
	resolveDBConfig()
	if db_passfile != "" {
		pgstring = fmt.Sprintf("user=%s passfile=%s host=%s database=%s",
			db_user,
			db_passfile,
			db_host,
			db_name)
	} else if db_pass != "" {
		pgstring = fmt.Sprintf("user=%s password=%s host=%s database=%s",
			db_user,
			db_pass,
			db_host,
			db_name)
	} else {
		pgstring = fmt.Sprintf("user=%s host=%s database=%s",
			db_user,
			db_host,
			db_name)
	}
	return
}

func getPostgresConnectionString() (pgstring string) {
	resolveDBConfig()
	if db_passfile != "" {
		pgstring = fmt.Sprintf("user=%s passfile=%s host=%s dbname=%s",
			db_user,
			db_passfile,
			db_host,
			db_name)
	} else if db_pass != "" {
		pgstring = fmt.Sprintf("user=%s password=%s host=%s dbname=%s",
			db_user,
			db_pass,
			db_host,
			db_name)
	} else {
		pgstring = fmt.Sprintf("user=%s host=%s dbname=%s",
			db_user,
			db_host,
			db_name)
	}
	return
}

func print_pg_connect_string() {
	fmt.Println(getPostgresConnectionString())
}
