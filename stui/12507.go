package main

import (
	"context"
	"fmt"
	"io"
	"os"
	"text/tabwriter"

	"github.com/jackc/pgx/v4"
)

// https://github.com/matrix-org/synapse/issues/12507

// test if you are affected by #12507
func issue12507(fixattempt bool) {
	fmt.Print("Testing if you are affected by #12507. Fix is ")
	if fixattempt {
		fmt.Print("en")
	} else {
		fmt.Print("dis")
	}
	fmt.Println("abled.")

	dburl := getPostgresConnectString()
	conn, err := pgx.Connect(context.Background(), dburl)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close(context.Background())

	events := make(map[string]string)
	rows, _ := conn.Query(context.Background(), "SELECT efe.event_id, efe.room_id FROM event_forward_extremities efe LEFT JOIN events e USING (event_id) WHERE e.event_id IS NULL;")

	innerRowScan := func(w io.Writer) {
		var event_id string
		var room_id string

		err := rows.Scan(&event_id, &room_id)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Scan failed: %v\n", err)
			os.Exit(1)
		}
		fmt.Fprintf(w, "%s\t%s\n", event_id, room_id)
		events[event_id] = room_id
	}

	if rows.Next() {
		fmt.Println("Rows found. Jup, it did happen to you:")
		w := tabwriter.NewWriter(os.Stdout, 6, 8, 1, ' ', 0)
		fmt.Fprintf(w, "%s\t%s\n", "event_id", "room_id")
		innerRowScan(w)
		for rows.Next() {
			innerRowScan(w)
		}
		w.Flush()
	} else {
		// no rows, yay \o/
		fmt.Println("You seem not affected by this issue. Yay \\o/")
		return
	}

	if rows.Err() != nil {
		fmt.Fprintf(os.Stderr, "Query failed: %v\n", rows.Err())
		os.Exit(1)
	}

	if !fixattempt {
		return
	}

	for e, r := range events {
		_, err := conn.Exec(context.Background(), "DELETE FROM event_forward_extremities WHERE event_id=$1", e)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error Deleting '%s': %v\n", e, err)
			os.Exit(1)
		}
		fmt.Printf("Event: %s  Room: %s\n", e, r)
	}
	fmt.Println("Synapse may have cached the deleted items, it is recommnded to restart Synapse to take the changes into effect.")
}
