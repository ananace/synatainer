package main

import (
	"fmt"

	"github.com/integrii/flaggy"
)

// no direct help call
//https://github.com/integrii/flaggy/issues/76

// Make a variable for the version which will be set at build time.
var version = "unknown"

// Keep subcommands as globals so you can easily check if they were used later on.
var (
	cmdCron                 *flaggy.Subcommand
	cmdCronhandler          *flaggy.Subcommand
	cmdDBShell              *flaggy.Subcommand
	cmdDBInfo               *flaggy.Subcommand
	cmdPGPassExport         *flaggy.Subcommand
	cmdInfo                 *flaggy.Subcommand
	cmdVacuum               *flaggy.Subcommand
	cmd12507                *flaggy.Subcommand
	cmdRoom                 *flaggy.Subcommand
	cmdRooms                *flaggy.Subcommand
	cmdAutoCompressor       *flaggy.Subcommand
	cmd13026                *flaggy.Subcommand
	cmdMXPassExport         *flaggy.Subcommand
	cmdMXPrintToken         *flaggy.Subcommand
	cmdPGPrintConnectString *flaggy.Subcommand
)

// Setup the variables you want your incoming flags to set.
var configFile string

var (
	db_user     string
	db_host     string
	db_name     string
	db_pass     string
	db_passfile string
)

var (
	mx_bearer_token string
	mx_synapse_host string
	mx_token_file   string
)

var vacuum_quiet bool
var vacuum_full bool
var sac_chunk_size string
var sac_chunks_to_compress string

// include tokens and passords in config info
var infoPrivate bool

var issue12507fixattempt bool
var issue13026fixattempt bool

var roomCmd string
var roomID string

func addDBParams(cmd *flaggy.Subcommand) {
	cmd.String(&db_user, "u", "user", "database user")
	cmd.String(&db_host, "s", "host", "datbase host")
	cmd.String(&db_name, "n", "name", "database name")
	cmd.String(&db_pass, "p", "password", "database pasword")
	cmd.String(&db_passfile, "", "pgpassfile", "pq-lib campatible pasword file")
}

func addMXParams(cmd *flaggy.Subcommand) {
	cmd.String(&mx_bearer_token, "t", "token", "Synapse admin token")
	cmd.String(&mx_synapse_host, "y", "synapsehost", "synapse host")
	cmd.String(&mx_token_file, "", "mxtokenfile", "matrix token file")
}

func init() {
	// Set your program's name and description.  These appear in help output.
	flaggy.SetName("stui")
	flaggy.SetDescription("Synatainer Terminal User Interface")

	// You can disable various things by changing bools on the default parser
	// (or your own parser if you have created one).
	flaggy.DefaultParser.ShowHelpOnUnexpected = true

	// You can set a help prepend or append on the default parser.
	flaggy.DefaultParser.AdditionalHelpPrepend = "You may find more information at https://gitlab.com/mb-saces/synatainer"

	// You can set a help prepend or append on the default parser.
	flaggy.DefaultParser.AdditionalHelpAppend = "\nStart stui without parameters or commands to obtain an interactive user interface"

	// global flags
	flaggy.String(&configFile, "c", "config", "Config file to use")

	cmdCron = flaggy.NewSubcommand("cron")
	cmdCron.Description = "Start the cron(like) background server"

	cmdCronhandler = flaggy.NewSubcommand("cronhandler")
	cmdCronhandler.Description = "replacement for the old cron script?"

	cmdInfo = flaggy.NewSubcommand("info")
	cmdInfo.Description = "Show configuration"
	cmdInfo.Bool(&infoPrivate, "p", "private", "Show private tokens and passwords")

	cmdDBShell = flaggy.NewSubcommand("db-shell")
	cmdDBShell.Description = "Open a database shell, psql like"
	addDBParams(cmdDBShell)

	cmdDBInfo = flaggy.NewSubcommand("db-info")
	cmdDBInfo.Description = "Prints some database info/stats"
	addDBParams(cmdDBInfo)

	cmdVacuum = flaggy.NewSubcommand("vacuum-db")
	cmdVacuum.Description = "Vacuum the whole database"
	addDBParams(cmdVacuum)
	cmdVacuum.Bool(&vacuum_quiet, "q", "quiet", "Be not verbose.")
	cmdVacuum.Bool(&vacuum_full, "f", "full", "Do a full vacuum.")

	cmdPGPassExport = flaggy.NewSubcommand("pgpassexport")
	cmdPGPassExport.Description = "Prints the line for the PGPASSFILE"
	addDBParams(cmdPGPassExport)

	cmd12507 = flaggy.NewSubcommand("12507")
	cmd12507.Description = "Tool for synapse issue #12507 (No state group for unknown or outlier event)"
	cmd12507.AdditionalHelpAppend = `
Tests if you are affected by this issue.
Use --fix to delete the bogous things.
More info about the issue: https://github.com/matrix-org/synapse/issues/12507
`
	addDBParams(cmd12507)
	cmd12507.Bool(&issue12507fixattempt, "f", "fix", "Delete bogous things.")

	cmdRoom = flaggy.NewSubcommand("room")
	cmdRoom.Description = "Show room details"
	addMXParams(cmdRoom)
	cmdRoom.AddPositionalValue(&roomCmd, "cmd", 1, true, "commands: detail members")
	cmdRoom.AddPositionalValue(&roomID, "room-id", 2, true, "room id")

	cmdRooms = flaggy.NewSubcommand("rooms")
	cmdRooms.Description = "List rooms"
	addMXParams(cmdRooms)
	cmdRooms.AddPositionalValue(&roomCmd, "cmd", 1, true, "commands: list")

	cmdAutoCompressor = flaggy.NewSubcommand("autocompressor")
	cmdAutoCompressor.Description = "Run the synapse_auto_compressor"
	addDBParams(cmdAutoCompressor)
	cmdAutoCompressor.String(&sac_chunk_size, "", "chunksize", "Chunk size, default 500")
	cmdAutoCompressor.String(&sac_chunks_to_compress, "", "chunkstocompress", "Chunks to compress, default 100")

	cmd13026 = flaggy.NewSubcommand("13026")
	cmd13026.Description = "Tool for synapse issue #13026 (No state group for unknown or outlier event)"
	cmd13026.AdditionalHelpAppend = `
Tests if you are affected by this issue.
Use --fix to delete the bogous things.
More info about the issue: https://github.com/matrix-org/synapse/issues/13026
`
	addDBParams(cmd13026)
	addMXParams(cmd13026)
	cmd13026.Bool(&issue13026fixattempt, "f", "fix", "Delete bogous things.")

	cmdMXPassExport = flaggy.NewSubcommand("mxpassexport")
	cmdMXPassExport.Description = "Prints the line for the MXPASSFILE"
	addMXParams(cmdMXPassExport)

	cmdMXPrintToken = flaggy.NewSubcommand("mxtoken")
	cmdMXPassExport.Description = "Prints the matrix token for shell scripts"

	cmdPGPrintConnectString = flaggy.NewSubcommand("pgconnectstring")
	cmdPGPrintConnectString.Description = "Prints the psql connect string for shell scripts"

	// Add the subcommand to the parser at position 1
	flaggy.AttachSubcommand(cmdCron, 1)
	flaggy.AttachSubcommand(cmdCronhandler, 1)
	flaggy.AttachSubcommand(cmdInfo, 1)
	flaggy.AttachSubcommand(cmdVacuum, 1)
	flaggy.AttachSubcommand(cmdDBShell, 1)
	flaggy.AttachSubcommand(cmdDBInfo, 1)
	flaggy.AttachSubcommand(cmdPGPassExport, 1)
	flaggy.AttachSubcommand(cmd12507, 1)
	flaggy.AttachSubcommand(cmdRoom, 1)
	flaggy.AttachSubcommand(cmdRooms, 1)
	flaggy.AttachSubcommand(cmdAutoCompressor, 1)
	flaggy.AttachSubcommand(cmd13026, 1)
	flaggy.AttachSubcommand(cmdMXPassExport, 1)
	flaggy.AttachSubcommand(cmdMXPrintToken, 1)
	flaggy.AttachSubcommand(cmdPGPrintConnectString, 1)

	// Set the version and parse all inputs into variables.
	flaggy.SetVersion(version)
	flaggy.Parse()
}

func main() {
	//	loadConfig()
	if cmdCron.Used {
		fmt.Println("Cron.")
	} else if cmdCronhandler.Used {
		fmt.Println("CronHandler.")
	} else if cmdVacuum.Used {
		vacuum_db(vacuum_full, vacuum_quiet)
	} else if cmdDBShell.Used {
		fmt.Println("psql emulator")
	} else if cmdDBInfo.Used {
		db_info()
	} else if cmdInfo.Used {
		show_config_info(infoPrivate)
	} else if cmdPGPassExport.Used {
		print_pgpass_line()
	} else if cmd12507.Used {
		issue12507(issue12507fixattempt)
	} else if cmdRoom.Used {
		doRoomCmd(roomCmd, roomID)
	} else if cmdRooms.Used {
		doRoomsCmd(roomCmd)
	} else if cmdAutoCompressor.Used {
		autocompressor()
	} else if cmd13026.Used {
		issue13026(flaggy.TrailingArguments, issue13026fixattempt)
	} else if cmdMXPassExport.Used {
		print_mxpass_line()
	} else if cmdMXPrintToken.Used {
		print_mxtoken()
	} else if cmdPGPrintConnectString.Used {
		print_pg_connect_string()
	} else {
		// empty command line, start tui
		//stui_tui()
		flaggy.ShowHelp("NoNoNo, Mr. TUI not here. Sorry, sooon…")
	}
}
