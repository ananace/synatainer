package mxadmin

import (
	"fmt"

	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/id"
)

type AdminClient struct {
	*mautrix.Client
}

func (cli *AdminClient) RoomDetail(room_id id.RoomID) (resp *RespRoomDetail, err error) {
	urlPath := cli.BuildAdminURL("v1", "rooms", string(room_id))
	_, err = cli.MakeRequest("GET", urlPath, nil, &resp)
	return
}

func (cli *AdminClient) RoomMembers(room_id id.RoomID) (resp *RespRoomMembers, err error) {
	urlPath := cli.BuildAdminURL("v1", "rooms", string(room_id), "members")
	_, err = cli.MakeRequest("GET", urlPath, nil, &resp)
	return
}

func (cli *AdminClient) ListRooms(from int, limit int) (resp *RespRoomList, err error) {
	urlData := AdminURLPath{"v1", "rooms"}
	queryParams := map[string]string{}
	queryParams["from"] = fmt.Sprintf("%d", from)
	queryParams["limit"] = fmt.Sprintf("%d", limit)
	urlPath := cli.BuildURLWithQuery(urlData, queryParams)
	_, err = cli.MakeRequest("GET", urlPath, nil, &resp)
	return
}

func (cli *AdminClient) RoomForwardExtremities(room_id id.RoomID) (resp *RespRoomForwardExtremities, err error) {
	urlPath := cli.BuildAdminURL("v1", "rooms", string(room_id), "forward_extremities")
	_, err = cli.MakeRequest("GET", urlPath, nil, &resp)
	return
}

// NewClient creates a new Matrix Client ready for syncing
func NewAdminClient(homeserverURL string, userID id.UserID, accessToken string) (*AdminClient, error) {
	client, err := mautrix.NewClient(homeserverURL, userID, accessToken)
	if err != nil {
		return nil, err
	}
	return &AdminClient{client}, nil
}
