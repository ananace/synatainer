package mxadmin

type AdminURLPath []interface{}

func (cup AdminURLPath) FullPath() []interface{} {
	return append([]interface{}{"_synapse", "admin"}, []interface{}(cup)...)
}

// BuildAdminURL builds a URL with the Client's homeserver and appservice user ID set already.
// This method also automatically prepends the synapse API prefix (/_synapse/admin).
func (cli *AdminClient) BuildAdminURL(urlPath ...interface{}) string {
	return cli.BuildURLWithQuery(AdminURLPath(urlPath), nil)
}
