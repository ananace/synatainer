package main

import (
	"os"
)

func getUserConfigDir() string {
	cfgDir, err := os.UserConfigDir()
	if err != nil {
		panic(err)
	}
	return cfgDir
}
