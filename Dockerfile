# syntax=docker/dockerfile:1

FROM docker.io/library/golang:1.18-alpine as builder

WORKDIR /app

COPY stui/go.mod ./
COPY stui/go.sum ./
RUN go mod download

COPY stui/ ./

ARG STUI_VERSION=dockerbuild
RUN go build -ldflags="-X main.version=$STUI_VERSION" -o /stui

FROM registry.gitlab.com/mb-saces/rust-synapse-tools:latest

RUN apk update --no-cache
RUN apk add --no-cache bash busybox-suid curl jq postgresql-client

COPY scripts/ /usr/local/bin

#sendmail from hack
COPY sendmail.wrapper /usr/local/sbin/sendmail

COPY entrypoint.sh /entrypoint.sh
COPY setup-crontab.sh /setup-crontab.sh

COPY --from=builder /stui /usr/local/bin/stui

ENTRYPOINT [ "/entrypoint.sh" ]
